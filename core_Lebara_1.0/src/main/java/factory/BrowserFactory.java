package factory;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.PageFactory;


public class BrowserFactory {
    protected WebDriver driver;

    public BrowserFactory() {
        initBrowser();
    }

    public WebDriver getChromeDriver(){
        return new ChromeDriver();
    }

    public WebDriver getDriver(){
        return this.driver;
    }

    private void initBrowser(){
        this.driver=new ChromeDriver();
    }
}
