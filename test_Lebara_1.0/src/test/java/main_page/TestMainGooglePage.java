package main_page;

import base.BasePage;
import org.junit.BeforeClass;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import page.MainGooglePage;


public class TestMainGooglePage {
    MainGooglePage page;
    @BeforeTest
    public void preparePage(){
        this.page = new MainGooglePage();
    }

    @AfterTest
    public void endTest() throws InterruptedException {
        Thread.sleep(5000);
        page.quitDriver();
    }

    @Test
    public void testMnePoshastit(){
        System.out.println("Sergey Zalevsky");
        page.searchMnePoshastit("Sergey Zalevsky");
    }
}
