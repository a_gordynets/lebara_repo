package page;

import base.BasePage;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

/**
 * Created by a.gordynets on 3/23/2017.
 */
public class MainGooglePage extends BasePage {
    @FindBy(xpath = "//*[@aria-label='Мне пашчасціць']")
    protected WebElement mnePoshastitButtonXpath;

    @FindBy(xpath = ".//*[@id='lst-ib']")
    protected WebElement searchField;

    public MainGooglePage(){
        //super();
        openURL("http://google.by");
        //PageFactory.initElements(driver,this);
    }

    public void clickMnePoshastitButton(){
        this.mnePoshastitButtonXpath.click();
    }

    public void searchMnePoshastit(String string){
        this.searchField.sendKeys(string);
        this.clickMnePoshastitButton();
    }

}
