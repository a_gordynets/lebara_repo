package base;


import factory.BrowserFactory;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

/**
 * Created by a.gordynets on 3/23/2017.
 */
public class BasePage {
    protected WebDriver driver;

    @FindBy(xpath = "//button[1]")
    private WebElement firstButton;

    @FindBy(xpath = "//a[1]")
    private WebElement firstLink;

    @FindBy(xpath = "//title")
    private WebElement title;

    public BasePage(){
        BrowserFactory factory = new BrowserFactory();
        this.driver = factory.getDriver();
        PageFactory.initElements(driver,this);
    }

    public WebDriver getDriver(){
        return driver;
    }

    public void openURL(String url){
        driver.get(url);
    }

    public void quitDriver(){
        driver.quit();
    }

    public void clickButtonByXpath(String xpath){
        driver.findElement(By.xpath(xpath));
    }

    public String getTitle(){
        return title.getText();
    }
}
